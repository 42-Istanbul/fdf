#include "fdf.h"

void	print_matrix(t_fdf *fdf)
{
	int	i;
	int	j;

	i = 0;
	while (i < fdf->height)
	{
		j = 0;
		while (j < fdf->width)
		{
			ft_printf("%3d", fdf->z_matrix[i][j]);
			++j;
		}
		ft_printf("\n");
		++i;
	}
}

int	main(int argc, char *argv[])
{
	t_fdf	*fdf;

	if (argc == 2)
	{
		fdf = (t_fdf *)malloc(sizeof(t_fdf));
		read_map(fdf, argv[1]);
		print_matrix(fdf);
		fdf->zoom = 20;
		create_win(fdf);
	}
	else
		ft_printf("Too many or few arguments\n");
	return (0);
}
