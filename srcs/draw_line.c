#include "fdf.h"
#define MAX(a, b) (a > b ? a : b)
#define ABS(a) ((a < 0) ? -a : a)

void	ft_swap(int *x, int *y)
{
	int	tmp;

	tmp = *x;
	*x = *y;
	*y = tmp;
}

void	isometric(int *x, int *y, int z)
{
	*x = (*x - *y) * cos(0.8);
	*y = (*x + *y) * sin(0.8) - z;
}

void	prep_xy(int *x, int *y, int *x1, int *y1, int *z, int *z1, t_fdf *fdf)
{
	*z = fdf->z_matrix[*y][*x];
	*z1 = fdf->z_matrix[*y1][*x1];
	*x *= fdf->zoom;
	*y *= fdf->zoom;
	*x1 *= fdf->zoom;
	*y1 *= fdf->zoom;
	fdf->color = (*z || *z1) ? 0xe80c0c : 0xffffff;
	isometric(x, y, *z);
	isometric(x1, y1, *z1);
	x += fdf->shift_x;
	y += fdf->shift_y;
	x1 += fdf->shift_x;
	y1 += fdf->shift_y;
}

void	bresenham(int x, int y, int x1, int y1, t_fdf *fdf)
{
	int		dx;
	int		dy;
	int		P;
	int		y_step;
	int		z;
	int		z1;
	char	steep;

	prep_xy(&x, &y, &x1, &y1, &z, &z1, fdf);
	dx = ABS(x1 - x);
	dy = ABS(y1 - y);
	steep = dy > dx;
	if (steep)
	{
		ft_swap(&x, &y);
		ft_swap(&x1, &y1);
		dx = ABS(x1 - x);
		dy = ABS(y1 - y);
	}
	if (x > x1)
	{
		ft_swap(&x, &x1);
		ft_swap(&y, &y1);
	}
	y_step = y1 > y ? 1 : -1;
	P = 2 * dy - dx;
	while (x <= x1)
	{
		if (steep)
			mlx_pixel_put(fdf->mlx, fdf->win, y, x, fdf->color);
		else
			mlx_pixel_put(fdf->mlx, fdf->win, x, y, fdf->color);
		if (P < 0)
			P += 2 * dy;
		else
		{
			P += 2*dy - 2*dx;
			y += y_step;
		}
		++x;
	}
}
/*
void	dda(float x1, float y1, float x2, float y2, t_fdf *fdf)
{
	float	x_step;
	float	y_step;
	int		max;
	int		z1;
	int		z2;

	z1 = fdf->z_matrix[(int)y1][(int)x1];
	z2 = fdf->z_matrix[(int)y2][(int)x2];

	x1 *= fdf->zoom;
	y1 *= fdf->zoom;
	x2 *= fdf->zoom;
	y2 *= fdf->zoom;

	fdf->color = (z1 || z2) ? 0xe80c0c : 0xffffff;

	isometric(&x1, &y1, z1);
	isometric(&x2, &y2, z2);
	x1 += fdf->shift_x;
	y1 += fdf->shift_y;
	x2 += fdf->shift_x;
	y2 += fdf->shift_y;

	x_step = x2 - x1;
	y_step = y2 - y1;
	max = MAX(ABS(x_step), ABS(y_step));
	x_step /= max;
	y_step /= max;
	while ((int)(x1 - x2) || (int)(y1 - y2))
	{
		mlx_pixel_put(fdf->mlx, fdf->win, x1, y1, fdf->color);
		x1 += x_step;
		y1 += y_step;
	}
}
*/
void	draw_map(t_fdf *fdf)
{
	int	x;
	int	y;

	y = 0;
	while (y < fdf->height)
	{
		x = 0;
		while (x < fdf->width)
		{
			if (x < fdf->width - 1)
				bresenham(x, y, x + 1, y, fdf);
				//dda(x, y, x + 1, y, fdf);
			if (y < fdf->height - 1)
				bresenham(x, y, x, y + 1, fdf);
				//dda(x, y, x, y + 1, fdf);
			++x;
		}
		++y;
	}
}
