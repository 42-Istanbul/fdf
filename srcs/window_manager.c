#include "fdf.h"

int	shift_keys(int key, t_fdf *fdf)
{
	ft_printf("%d\n", key);
	// Up
	if (key == 65362)
		fdf->shift_y -= 10;
	// Down
	if (key == 65364)
		fdf->shift_y += 10;
	// Left
	if (key == 65361)
		fdf->shift_x -= 10;
	// Right
	if (key == 65363)
		fdf->shift_x += 10;
	mlx_clear_window(fdf->mlx, fdf->win);
	draw_map(fdf);
	return (0);
}

void	custom_mlx_pixel_put(t_data *img, int x, int y, int color)
{
	char	*dst;

	dst = img->addr + (y * img->line_len + x * (img->bpp / 8));
	*(unsigned int *)dst = color;
}

void	create_win(t_fdf *fdf)
{
	fdf->mlx = mlx_init();
	fdf->win = mlx_new_window(fdf->mlx, 800, 600, "FdF");
	//fdf->img.img = mlx_new_image(fdf->mlx, 800, 600);
	//fdf->img.addr = mlx_get_data_addr(fdf->img.img, &fdf->img.bpp, &fdf->img.line_len,
	//		&fdf->img.endian);
	// custom_mlx_pixel_put(&fdf->img, 10, 10, 0x00FF00FF);
	//mlx_put_image_to_window(fdf->mlx, fdf->win, fdf->img.img, 0, 0);
	draw_map(fdf);
	//bresenham(50, 50, 100, 100, fdf);
	mlx_key_hook(fdf->win, shift_keys, fdf);
	mlx_hook(fdf->win, 2, 1L<<0, close_win, fdf);
	mlx_loop(fdf->mlx);
}

int	close_win(int keycode, t_fdf *fdf)
{
	if (keycode == 65307)
		mlx_destroy_window(fdf->mlx, fdf->win);
	return (0);
}
