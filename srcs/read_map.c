#include "fdf.h"

static void	read_row(int *z_matrix, char *line)
{
	char	**split;
	int		j;

	j = 0;
	split = ft_split(line, ' ');
	while (split[j])
	{
		z_matrix[j] = ft_atoi(split[j]);
		++j;
	}
	free(split);
}

static void	read_to_matrix(t_fdf *fdf, char *fname)
{
	char	*line;
	int		fd;
	int		i;

	i = 0;
	fdf->z_matrix = (int **)malloc(sizeof(int *) * (fdf->height + 1));
	while (i < fdf->height)
		fdf->z_matrix[i++] = (int *)malloc(sizeof(int) * (fdf->width + 1));
	fd = open(fname, O_RDONLY);
	if (fd < 1)
		handle_error();
	line = get_next_line(fd);
	i = 0;
	while (line)
	{
		read_row(fdf->z_matrix[i], line);
		line = get_next_line(fd);
		++i;
	}
	free(line);
	close(fd);
	fdf->z_matrix[i] = NULL;
}

void	read_map(t_fdf *fdf, char *fname)
{
	char	*line;
	int		fd;

	fd = open(fname, O_RDONLY);
	if (fd < 1)
		handle_error();
	line = get_next_line(fd);
	fdf->width = ft_count_words(line, ' ');
	while (line)
	{
		line = get_next_line(fd);
		++fdf->height;
	}
	free(line);
	close(fd);
	read_to_matrix(fdf, fname);
}
