#ifndef FDF_H
# define FDF_H

# include <stdio.h>
# include <errno.h>
# include <string.h>
# include <fcntl.h>
# include <math.h>
# include "../minilibx/mlx.h"
# include "../libft/includes/libft.h"

# define STDIN 0
# define STDOUT 1
# define STDERR 2

typedef struct	s_data
{
	void	*img;
	char	*addr;
	int		bpp;
	int		line_len;
	int		endian;
}				t_data;

typedef struct	s_fdf
{
	int	width;
	int	height;
	int	**z_matrix;
	int	zoom;
	int	color;
	int	shift_x;
	int	shift_y;

	void	*mlx;
	void	*win;
	t_data	img;
}				t_fdf;

void	handle_error(void);
void	print_matrix(t_fdf *fdf);
void	read_map(t_fdf *fdf, char *fname);

void	create_win(t_fdf *fdf);
void	custom_mlx_pixel_put(t_data *img, int x, int y, int color);
int		close_win(int keycode, t_fdf *fdf);

void	dda(float x1, float y1, float x2, float y2, t_fdf *fdf);
void	bresenham(int x, int y, int x1, int y1, t_fdf *fdf);
void	draw_map(t_fdf *fdf);

#endif
