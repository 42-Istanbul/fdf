CC = gcc
CFLAGS = -Wall -Werror -Wextra -g
MLX_FLAGS = -Lminilibx -lmlx -L/usr/bin/lib/ -lXext -lX11 -lm
RM = rm -rf

NAME = fdf.out
INCLUDES = -I includes
MLX_DIR = minilibx
LIBFT_DIR = libft
LIBFT_NAME = libft.a
SRC_DIR = srcs
OBJ_DIR = objs
SRC_FILES = fdf.c read_map.c errors.c window_manager.c draw_line.c
OBJ_FILES = $(SRC_FILES:.c=.o)
SRCS = $(addprefix $(SRC_DIR)/, $(SRC_FILES))
OBJS = $(addprefix $(OBJ_DIR)/, $(OBJ_FILES))

all: $(NAME)

$(NAME): $(LIBFT_DIR)/$(LIBFT_NAME) $(OBJ_DIR) $(OBJS)
	$(MAKE) -sC $(MLX_DIR)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME) -L$(LIBFT_DIR) -lft $(MLX_FLAGS) $(INCLUDES)

$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDES)

$(LIBFT_DIR)/$(LIBFT_NAME):
	$(MAKE) -sC $(LIBFT_DIR)

clean:
	$(MAKE) -sC $(MLX_DIR) clean
	$(MAKE) -sC $(LIBFT_DIR) clean
	$(RM) $(OBJ_DIR)

fclean: clean
	$(MAKE) -sC $(LIBFT_DIR) fclean
	$(RM) $(NAME)

re: fclean all

.PHONY: all, clean, fclean, re
